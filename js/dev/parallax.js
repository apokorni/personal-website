(function () {
	'use strict';

	var doc, win, parallax, moveElem, scrolltop, scalling;

	doc = document;
	win = window;

	moveElem = doc.getElementsByClassName('teaser__Title')[0];

	win.requestAnimationFrame = win.requestAnimationFrame || win.mozRequestAnimationFrame || win.webkitRequestAnimationFrame || win.msRequestAnimationFrame || function(f) { setTimeout(f, 1000 / 60); };

	parallax = function() {

		/**

			TODO:
			- webkit browseri imaju bug kod animiranja translateY propertyja,
			- za sada translate3d riješava taj problem, testirat

		**/

		scrolltop = win.pageYOffset;
		moveElem.style.transform = "translate3d(0," + ( scrolltop * 0.49) + "px, 0)";

	};

	win.addEventListener('DOMContentLoaded', function() {

		moveElem.style.transform = "translate3d(0,0,0)";

	}, false);

	win.addEventListener('scroll', function () {

		requestAnimationFrame(parallax);

	}, false);

})();
