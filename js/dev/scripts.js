document.addEventListener('DOMContentLoaded', function () {
	'use strict';

	var _win, _doc, _body, menuBtn, siteHeader, siteWrapper, overlay, htmlBody;
	_win = window;
	_doc = document;
	_body = _doc.body;
	htmlBody = $('body, html');
	overlay = _doc.getElementsByClassName('js-Overlay')[0];
	menuBtn = _doc.getElementsByClassName('js-menuBtn')[0];
	siteHeader = _doc.getElementsByClassName('siteHeader')[0];
	//siteWrapper = _doc.getElementsByClassName('js-siteWrapper')[0];

	//Menu btn click
	menuBtn.addEventListener('click', function () {
		siteHeader.classList.toggle('siteHeader--isTransformed');
		this.classList.toggle('menuBtn--isActive');
		overlay.classList.toggle('Overlay--isHidden');
		//siteWrapper.classList.toggle('siteWrapper--isTransformed');
	});

	//Scroll to top
	$('.js-toTop__Btn').on('click',  function () {
		htmlBody.animate({
				scrollTop: 0
		}, 800);
		return false;
	});

	//Smooth Scroll
	$('a[href^="#"]').click(function (e) {
		e.preventDefault();
		htmlBody.animate({ scrollTop: $(this.hash).offset().top}, 600);
		return false;
	});

	//Hide overlay
	$(_body).on('click', function (event) {
		if ($(event.target).closest('.js-Overlay').length > 0) {

			siteHeader.classList.add('siteHeader--isTransformed');
			//siteWrapper.classList.remove('siteWrapper--isTransformed');
			$(menuBtn).removeClass('menuBtn--isActive');
			$(overlay).addClass('Overlay--isHidden');

		}
	});

	// window onload event
	// _win.addEventListener('load', function() {

	// 	_body.classList.add('body-class');

	// 	typed.js
	// 	$('.js-teaser__Title').typed({
	// 		strings: ['I\'m a freelance web developer'],
	// 		typeSpeed: 60,
	// 		showCursor: true,
	// 		startDelay: 1000
	// 	});

	// });

	// Scroll to top function
	function getScrollTop () {

		if (typeof pageYOffset != 'undefined') {
			//most browsers except IE before #9
			return pageYOffset;
		} else {
			var docBody, docElement;

			docBody = doc.body; // IE hack
			docElement = doc.documentElement; //IE hack

			docElement = (docElement.clientHeight) ? docElement : docBody;
			return docElement.scrollTop;
		}
	}

	var portfolioGraphics = _doc.getElementsByClassName('js-portfolio__Graphics')[0];
	var portfolioGraphicsBtn = _doc.getElementsByClassName('js-portfolioBtn__Graphics')[0];

	$(portfolioGraphics).hide();

	$(portfolioGraphicsBtn).on('click', function () {

		$(portfolioGraphics)
			.slideToggle('fast');
			htmlBody.animate({ scrollTop: $(this.hash).offset().top}, 600);

	});

	// Disable contacts submit button
	var btnSubmit = _doc.getElementsByClassName('contact__submit')[0];

	btnSubmit.setAttribute('disabled', true);

	if ( btnSubmit.hasAttributes('disabled') === true ) {
		btnSubmit.classList.add('contact__submit--isDisabled');
	}

	// Hide Heroku section
	$('.heroku').hide();


	// Scroll event
	var timer;

	_win.addEventListener('scroll', function() {

		if (timer) {
			_win.clearTimeout(timer);
		}

		timer = _win.setTimeout(function () {
			console.log( 'Firing' );
		}, 100);

		//siteWrapper.classList.remove('siteWrapper--isTransformed');

		setTimeout(function() {

			siteHeader.classList.add('siteHeader--isTransformed');
			menuBtn.classList.remove('menuBtn--isActive');
			overlay.classList.add('Overlay--isHidden');

		}, 50);


		// if( getScrollTop() >= 907 ) {
		// 	menuBtn.classList.add('menuBtn--isColored');
		// } else {
		// 	menuBtn.classList.remove('menuBtn--isColored');
		// }

		var fixedHeader = _doc.getElementsByClassName('js-siteHeader')[0];

		//$('.js-fg').hide();

		// if ($('body').height() <= ($(_win).height() + $(_win).scrollTop())) {
		// 	$('.fg').fadeIn('slow');
		// } else {
		// 	$('.fg').fadeOut('slow');
		// }

	});


	// _win.addEventListener('resize', function () {

	// 	console.log('window is resizing');

	// });



}, false);