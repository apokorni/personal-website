(function () {
	'use strict';
	/*
	Snazzymaps styles
	themeStyle, blueWater, paleDawn, appleMaps, gowalla, brightAndBlubby, flatMap, brightDesert, orange, title, black, lgray, shadesOfGray, darkWorld, fuse
	*/

	function initialize() {
	var  map, icon, marker, daruvar, mapOptions;

	daruvar = new google.maps.LatLng(45.592835, 17.223761);

	mapOptions = {
		zoom: 15,
		draggable: true,
		//zoomControl: true,
		styles: shadesOfGray,
		center: daruvar,
		scrollwheel: false,
		disableDefaultUI: true
	};

	icon = {
		url: 'img/map-marker-blue.png' };

	map = new google.maps.Map(document.getElementsByClassName('js-map')[0], mapOptions);

	marker = new google.maps.Marker({
		map: map,
		//draggable: true,
		animation: google.maps.Animation.DROP,
		position: daruvar,
		icon: icon
	});

	google.maps.event.addListener(marker, 'click', toggleBounce);

}

function toggleBounce() {

	if (marker.getAnimation() !== null) {
		marker.setAnimation(null);
	} else {
		marker.setAnimation(google.maps.Animation.BOUNCE);
	}

}


	google.maps.event.addDomListener(window, 'load', initialize);

})();