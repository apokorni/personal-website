/*

Name: Alen Pokorni
Description: Simple Super Mario web game written with JS/jQuery 

*/

// function isCollide(a, b) {
//     return !(
//         ((a.y + a.height) < (b.y)) ||
//         (a.y > (b.y + b.height)) ||
//         ((a.x + a.width) < b.x) ||
//         (a.x > (b.x + b.width))
//     );
//     console.log("Collide");
// }

//-----------------------------------------------------------o
//	Globalne varijable
//-----------------------------------------------------------o

var $document    = $(document),
		$mario 			 = $('.mario'),
		$playground  = $('#playground'),
		$movesky 		 = $('.hills, .sun'),
		$moveGround  = $('#grid'),
		cannon   		 = $('#cannon'),
		questbox		 = $('.questbox'),
		$overlay     = $('#overlay'),
		successGame  = $('.success-game'),
		gameOver     = $('.gameOver'),
 		end_game 		 = 0, 
 		counter 		 = 0, 
 		s 					 = 35,
 		hit_list;

 		
 		
		var loaded = setTimeout(function() {
				$('#loader').toggleClass('isVisible isHidden');
			}, 1000);


		//-----------------------------------------------------------o 
//	Mario appears
//-----------------------------------------------------------o	
  function marioAppears() {
  	setTimeout(function(){
  		$mario
				.toggleClass('marioIsHidden marioIsVisible')
				.addClass('move-mario');
		}, 1000);
		cannon.addClass('canonAnim');
  }

//-----------------------------------------------------------o 
//	Sound
//-----------------------------------------------------------o	
	var theme = $('#themeSound')[0],
			flag  = $('#flagSound')[0];

	function themeSound() { theme.play(); }

	function themeSoundStop() { theme.pause(); }

	function jumpSound() {
		var audio = $('#marioJump')[0];
		audio.play();
	}

	function coinSound() {
		var audio = $('#coinSound')[0];
		audio.play();
	}

	function flagSound() { flag.play(); }

	function flagSoundStop() { flag.pause(); }

	function gameOverSound() {
		var audio	= $('#gameOverSound')[0];
		audio.play();
	}

	function fireballSound() {
		var audio	= $('#fireballSound')[0];
		audio.play();
	}

	
//-----------------------------------------------------------o
//	start game
//-----------------------------------------------------------o
$('#start').on('click', function(event) {
	event.preventDefault();
	$('#gsw').fadeOut('slow');
	marioAppears();
	setTimeout(function() {
		themeSound();
		$mario.removeClass('move-mario');
	}, 2000);
});	
		



(function () {
	
	//-----------------------------------------------------------o 
	//	Reload after game is finished
	//-----------------------------------------------------------o

  $('#btn, #btn-reset').on('click', function() {
  	setTimeout(function() {
  		location.reload(true)
  	}, 500);
  });


})();


	

$document.ready(function() {

//-----------------------------------------------------------o 
//	Preloader
//-----------------------------------------------------------o
	loaded;


$document.on('keydown', function(event) {
// function keypressed() {
	event.preventDefault();

	var key = event.KeyCode || event.which || event.key;
	hit_list = $mario.collision(questbox);
	cannon_hit = cannon.collision($mario);


	//-----------------------------------------------------------o 
	//	End game function
	//-----------------------------------------------------------o
	function eofGame() {
		end_game = $mario.collision('.zastava-outer');
			if ( end_game.length != 0  && counter >= 3 ) {
				$('.zastava-outer').addClass('zas-anim'); 
				flagSound();
				themeSoundStop();
				$overlay.delay(1000).fadeIn('slow', function() {
					$(this).addClass('isVisible').removeClass('isHidden');
				});
				
				successGame.addClass('isVisible').removeClass('isHidden');
				gameOver.css('display', 'none');
				cannon.removeClass('canonAnim');
				$document.off('keydown');
			} 
	}

	//-----------------------------------------------------------o 
	//	hit test
	//-----------------------------------------------------------o	
	function hitTest() {
		if ( hit_list.length != 0 ) {
	  	counter++;
	  	coinSound();
	  	$('#counter').empty().append(' ' + counter );
	  	$('#counter-2').empty().append(' ' + counter );

	  	hit_list.toggleClass('qbox-down qbox-up');
	  	setTimeout(function() {
	  		hit_list.toggleClass('qbox-up qbox-down');
	  	}, 100);
	  		
	  	hit_list.find('.coin').toggleClass('coin-down coin-up');
	  	setTimeout(function() {
	  		hit_list.find('.coin').toggleClass('coin-up coin-down');
	  	}, 250);
		}
	}	

	function cannonHit() {
		if ( cannon_hit.length != 0 ) {
	  	counter++;
	  	themeSoundStop();
	  	gameOverSound();

				$overlay.delay(1000).fadeIn('slow', function() {
					$(this).addClass('isVisible').removeClass('isHidden');
				});
				gameOver.addClass('isVisible').removeClass('isHidden');	
				successGame.css('display', 'none');
				cannon.removeClass('canonAnim');
		}
	}


	

	//-----------------------------------------------------------o 
	//	d - tipka desno
	//-----------------------------------------------------------o
		if ( key == 68 ) {
		//$mario.empty();
		//$mario.html(' <img src="animations/m-right.gif" /> ');
		$('#mario-character').removeClass('mario-character--left').addClass('mario-character');
		$mario.stop(true, false).animate({
    	left: '+=' + s }, 50, function() {
      	//$mario.html(' <img src="animations/m-right-stop.gif" /> ');
     	});
		$movesky.stop(true,false).animate({ left: '-=0.25',}, 0);
		$moveGround.stop(true,false).animate({ left: '-=0.75',}, 0);
	}


	if ( key == 65 ) {
		//$mario.empty();
		//$mario.html('<img src="animations/m-left.gif"/>');
		$('#mario-character').removeClass('mario-character').addClass('mario-character--left');
		$mario.stop(true, false).animate({
      left: '-=' + s }, 50, function() {
        //$mario.html('<img src="animations/m-left-stop.gif" />');
      });
		$movesky.stop(true,false).animate({ left: '+=0.25', }, 0);
		$moveGround.stop(true,false).animate({ left: '+=0.75',}, 0);
	}

	// // if ( key == 32 ) {
	// // 	$mario.addClass('jump');
	// // 	setTimeout(function(){
	// // 		$mario.stop(true).animate({
 // //      left: '+=50' }, 10, function() {
 // //      });
	// // 	}, 250);
	// // 	setTimeout(function() {
	// // 		$mario.removeClass('jump');
	// // 		//document.getElementById("myelement").className = "myclass";
	// // 	}, 500);
	// // 	//$mario.stop(true, false).animate({left: '+=50'});
		

	// // 		//jumpSound();
	// // } 

	if ( key == 87 ) {
		$mario.addClass('jump-wrp');
		if ( $('#mario-character').hasClass('mario-character--left') ) {
			$('#mario-character').addClass('jump--left');
			setTimeout(function() {
				$mario.removeClass('jump-wrp');
				$('#mario-character').removeClass('jump--left');
			}, 500);
		} else {
			$('#mario-character').addClass('jump');
			setTimeout(function() {
				$mario.removeClass('jump-wrp');
				$('#mario-character').removeClass('jump');
			}, 500);
		}

		jumpSound();
	}

	///////////////////////////

		function loop() {
		   cannon
		     .animate({right:900},3000)
		     .animate({right:80},3000, function(){loop();});
		  }
  
  //loop();
	// var d = document;
	// var a = d.getElementById("cannon");
	// var b = d.getElementById("mario");

	// console.log(isCollide($mario, cannon));
	eofGame();
	hitTest();
	cannonHit();
});

//-----------------------------------------------------------o
//	Document keypress
//-----------------------------------------------------------o
//$(this).stop(true,false).keydown(function(event) { keypressed(); });


//-----------------------------------------------------------o 
//	Playground
//-----------------------------------------------------------o
			var numRows = 100,
					numCols = 15,
					colors = ['#8B5A00','#CD8500', '#CD8500', '#8B5A00', '#8B4500', '#8B5A00','#CD8500', '#CD8500', '#8B5A00', '#8B4500'];

			// OUTER LOOP - rows		
			for ( var i = 0; i < numRows; i++ ) {
				var column = document.createElement('div');
				column.setAttribute('class', 'column');

					// INNER LOOP - elements in row
					for ( var j = 0; j < numCols; j++ ) {
						var box = document.createElement('div');
						box.setAttribute('class', 'box col' + j);
						column.appendChild(box);
					} // EOF INNER LOOP
				
				//$('#grid').append(column);  // ADDING COLUMNS INTO THE GRID

			} // EOF OUTER LOOP

			// ADDING COLORS INTO BOXES
			for ( var i = 0; i < colors.length; i++ ) {
				$('.col' + i*2).css('background-color', colors[i]);		
			}

//-----------------------------------------------------------o 
//	Hills
//-----------------------------------------------------------o
	// OUTER LOOP - rows		
			for ( var i = 0; i < 1; i++ ) {
				var column = document.createElement('div');
				column.setAttribute('class', 'Hills');

					// INNER LOOP - elements in row
					for ( var j = 0; j < 5; j++ ) {
						var boxHills = document.createElement('div');
						boxHills.setAttribute('class', 'hilll' + ' ' + 'hilll-' + j);
						column.appendChild(boxHills);
					} // EOF INNER LOOP
				
				$('#experiment').append(column);  // ADDING COLUMNS INTO THE GRID

			} // EOF OUTER LOOP
	
}); // EOF DOMReady

