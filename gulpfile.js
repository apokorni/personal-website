var gulp = require('gulp'),
		sass = require('gulp-sass'),
		browserSync = require('browser-sync'),
		notify = require('gulp-notify'),
		autoprefixer = require('gulp-autoprefixer'),
		filter = require('gulp-filter'),
		plumber = require('gulp-plumber'),
		cmq = require('gulp-combine-media-queries'),
		uncss = require('gulp-uncss'),
		uglify = require('gulp-uglifyjs'),
		svgstore = require('gulp-svgstore'),
		iconfont = require('gulp-iconfont'),
		consolidate = require('gulp-consolidate'),
		neat = require('node-neat').includePaths,

		loc_scss = "sass/**/*.scss",
		loc_html = "html/**/*.html",

		jsFilter = filter('**/*.js'),
		htmlFilter = filter('**/*.html'),
		scssFilter = filter('**/*.scss'),
		cssFilter = filter('**/*.css');

gulp.task('reload-html', function() {
	browserSync.reload();
});

// UglifyJS with bower components
gulp.task('uglify', function() {
	gulp.src([
			'js/lib/**/*.js',
			'js/dev/**/*.js'
		])
		.pipe(uglify('scripts.min.js', {
			outSourceMap: true
		}))
		.pipe(gulp.dest('js/min'))
});

// SVG to font
gulp.task('iconfont', function() {
	gulp.src(['icons/svg/*.svg'])
	.pipe(iconfont({
		fontName: 'univ',
		normalize: true,
		fontHeight: 500
	}))
	.on('codepoints', function(codepoints, options) {
		codepoints.forEach(function(glyph, idx, arr) {
			arr[idx].codepoint = glyph.codepoint.toString(16)
			});
		gulp.src('sass/templates/_iconfont.scss')
		.pipe(consolidate('lodash', {
			glyphs: codepoints,
			fontName: options.fontName,
			fontPath: '../icons/fonts/'
			}))
		.pipe(gulp.dest('sass/modules'));
		})
	.pipe(gulp.dest('icons/fonts'));
});

// Compile SASS
gulp.task('sass', function() {
	return gulp.src(loc_scss)
		.pipe(plumber())
		.pipe(sass({
			includePaths: ['styles'].concat(neat),
			errLogToConsole: true,
			onError: function(err) {
				return notify().write(err);
			}
		}))
		.pipe(autoprefixer({browsers: ['last 2 versions']}))
		.pipe(cmq())
		.pipe(gulp.dest('css'))
		.pipe(browserSync.reload({
			stream: true
		}));
});

gulp.task('browser-sync', function() {
	browserSync.init(["html/**/*.html", "js/**/*.js", ], {
		proxy:"http://wordpress.ia/personal-web",
		ghostMode: false,
		browser: ["chrome"],
		notify: true
	});
});

gulp.task('watch', function() {
	gulp.watch('js/dev/*.js', ['uglify']);
	gulp.watch('js/plugins/*.js', ['uglify']);
	gulp.watch([loc_html], ['reload-html']);
	gulp.watch(loc_scss, ['sass']);
	gulp.watch("**/*.php", ['reload-html']);
});

gulp.task('default', ['sass', 'browser-sync', 'reload-html', 'watch']);
